from argparse import ArgumentParser
import pyperclip

def run(args, cache_filename):
    cache = set()
    try:
        with open(cache_filename, "r", encoding="utf-8") as fh:
            for line in fh.readlines():
                line = line.rstrip()
                cache.add(line)
        print(f"Read {len(cache)} entries from {cache_filename}")
    except FileNotFoundError:
        pass

    data = pyperclip.paste()
    num_lines = data.count("\n")
    print(f"Read {num_lines} lines from clipboard")
    new_data = ""
    for line in data.split("\n"):
        # Find the tab
        i = line.find("\t")
        if i == -1:
            continue
        lhs = line[0:i].strip()
        rhs = line[i+1:].strip()
        new_item = f"{lhs}\t{rhs}"
        if new_item not in cache:
            new_data += f"{new_item}\n"
            cache.add(new_item)
    pyperclip.copy(new_data)

    num_lines = new_data.count("\n")
    if args.n:
        print(f"(-n) Would write {num_lines} lines to clipboard")
        print(f"(-n) Would write {len(cache)} entries to {cache_filename}")
        return

    print(f"Wrote {num_lines} lines to clipboard")

    if num_lines > 0:
        with open(cache_filename, "w", encoding="utf-8") as fh:
            for item in sorted(cache):
                print(item, file=fh)
        print(f"Wrote {len(cache)} entries to {cache_filename}")
    else:
        print(f"Didn't update {cache_filename}")

def main():
    parser = ArgumentParser()
    parser.add_argument("cache_file")

    parser.add_argument("-n",
        action="store_true",
        help="Don't make any changes")

    args = parser.parse_args()

    run(args, args.cache_file)

if __name__ == "__main__":
    main()
