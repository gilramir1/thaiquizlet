
from argparse import ArgumentParser

def run(args, text_file):
    lines = []
    with open(text_file, "r", encoding="utf-8") as fh:
        for line in fh.readlines():
            line = line.rstrip()
            lines.append(line)

    got_date = False
    got_title = False

    if text_file.endswith(".txt"):
        new_file = text_file[:-4] + ".kru.txt"
    else:
        new_file = text_file + ".kru.txt"

    with open(new_file, "w", encoding="utf-8") as fh:
        for line in lines:
            if line == "":
                print(line, file=fh)
                continue

            if not got_date:
                print(line, file=fh)
                got_date = True
                continue

            if not got_title:
                print(line, file=fh)
                print(file=fh)
                got_title = True
                continue

            print(line, file=fh)
            print("** " + line, file=fh)
            print(file=fh)

def main():
    parser = ArgumentParser()
    parser.add_argument("text_file")

    args = parser.parse_args()

    run(args, args.text_file)

if __name__ == "__main__":
    main()
